---?color=linear-gradient(to bottom right, #ffffff, #ffffff, #f0ad27, #0d048b)
@title[Introduction]

@snap[west headline text-marino span-85]
Estudio de motivos tipo DOR y su relación con la propiedad de robustez en redes
@snapend

@snap[south-west byline  text-white span-40]
Programa de maestría y doctorado en CIENCIAS BIOQUÍMICAS
@snapend

@snap[south-east byline  text-white span-40]
Roberto Galindo Ramírez
@snapend

+++?image=template/img/bg/black.jpg&position=right&size=50% 100%
@title[Comité tutoral]

@snap[east split-screen-list2-li text-white span-45](false)
Comité tutor
@ul
- Dr. Gabriel del Río Guerra
@size[0.5em](Instituto de Fisiología Celular)
<br>
- Dra. Martha Verónica Vázquez Laslop
@size[0.5em](Instituto de Biotecnología)
<br>
- Dr. Enrique Merino Pérez
@size[0.5em](Instituto de Biotecnología)
@ulend
@snapend

@snap[west split-screen-byline text-marino]
Tesis para optar por el grado de **Maestro en Ciencias Bioquímicas**
@snapend

@snap[south-west template-note text-gray]
Estudio de motivos tipo DOR y su relación con la propiedad de robustez en redes
@snapend

+++?color=linear-gradient(to bottom right, #ffffff, #ffffff, #f0ad27, #0d048b)
@title[Outline]

@snap[north-west]
Outline
@snapend

@snap[south-west list-content-concise span-100]
@ol
- Introducción
- Hipótesis
- Objetivos
- Métodos y materiales
- Resultados
- Discusión
- Conclusiones
@olend
<br><br>
@snapend

@snap[south-west template-note text-gray]
Estudio de motivos tipo DOR y su relación con la propiedad de robustez en redes.
@snapend
---
@title[Tip! Fullscreen]

![TIP](template/img/tip.png)
<br>
For the best viewing experience, press F for fullscreen.

`$A =\{(i,j): i \in V, j \in V\}$`

---?include=template/md/introduccion/PITCHME.md

---?include=template/md/split-screen/PITCHME.md

---?include=template/md/sidebar/PITCHME.md

---?include=template/md/list-content/PITCHME.md

---?include=template/md/image/PITCHME.md

---?include=template/md/sidebox/PITCHME.md

---?include=template/md/code-presenting/PITCHME.md

---?include=template/md/header-footer/PITCHME.md

---?include=template/md/quotation/PITCHME.md

---?include=template/md/announcement/PITCHME.md

---?include=template/md/about/PITCHME.md

---?include=template/md/wrap-up/PITCHME.md

---
@title[Final]

@snap[west headline span-100]
*Gracias*
@snapend

@snap[south docslink span-100]
Para mayor información consultar la [página del laboratorio](http://www.ifc.unam.mx/investigadores/gabriel-delrio)
@snapend
