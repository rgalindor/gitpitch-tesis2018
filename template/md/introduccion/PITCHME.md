---?image=template/img/spotlight.png&position=top right&size=20% auto
@title[Introducción]

## @color[black](Introducción<br>)

@fa[arrow-down text-black]


+++?image=template/img/bg/blue.jpg&position=right&size=50% 100%
@title[Sistemas]

@snap[west split-screen-heading text-blue span-50]
Sistemas
@snapend

@snap[east text-white span-45]
@ul[split-screen-list](false)
- **Integración de elementos**
- **Estados**
- Propiedades y características de los elementos
- **Interacción entre elementos**
- Cambio
@ulend
@snapend

@snap[south-west template-note text-gray]
Conceptos teóricos.
@snapend

+++?image=template/img/bg/blue.jpg&position=right&size=50% 100%
@title[Integración]

@snap[east split-screen-byline text-white]
@ul[split-screen-list](false)
- @color[#0D048B](**Integración de elementos**)
- **Estados**
- Propiedades y características de los elementos
- **Interacción entre elementos**
- Cambio
@ulend
@snapend

@snap[west split-screen-img]
![Integración](template/img/Figuras/Preparacion/redMinima.png)

@snapend

@snap[south-west template-note text-gray]
Conceptos teóricos.
@snapend

+++?image=template/img/bg/blue.jpg&position=right&size=50% 100%
@title[Estados]

@snap[east split-screen-byline text-white]
@ul[split-screen-list](false)
- **Integración de elementos**
- @color[#0D048B](**Estados**)
- Propiedades y características de los elementos
- **Interacción entre elementos**
- Cambio
@ulend
@snapend

@snap[west split-screen-byline]
@fa[circle fa-2x text-pink]
<br>
@fa[square fa-spin text-green]
<br>
@fa[heart fa-3x text-blue]
@snapend

@snap[south-west template-note text-gray]
Conceptos teóricos.
@snapend


+++?image=template/img/bg/blue.jpg&position=right&size=50% 100%
@title[Interacción]

@snap[east split-screen-byline text-white]
@ul[split-screen-list](false)
- **Integración de elementos**
- **Estados**
- Propiedades y características de los elementos
- @color[#0D048B](**Interacción entre elementos**)
- Cambio
@ulend
@snapend

@snap[west split-screen-img]
<!--![Integración](template/img/Figuras/network.gif)-->
![Integración](https://media.giphy.com/media/3nrnKH9wRoRAA/giphy-downsized.gif)
@snapend

@snap[south-west template-note text-gray]
Conceptos teóricos.
@snapend


+++?image=template/img/bg/blue.jpg&position=top&size=100% 20%
@title[Robustez]

@snap[north text-white span-100]
@size[2em](Robustez)
@snapend

@snap[souuth split-screen-img]
![robustness](template/img/Figuras/robust.png)
@snapend

@snap[south-west template-note text-gray]
Conceptos teóricos
@snapend

+++?image=template/img/bg/blue.jpg&position=top&size=100% 20%
@title[Transcripción]

@snap[north text-white span-100]
@size[1.2em](Transcripción en eucariontes)
@snapend

@snap[midpoint split-screen-bigImg]
![transcription](template/img/Figuras/eukTranscriptionBLue.png)
@snapend

@snap[south-west template-note text-gray]
Transcripción.
@snapend

+++?image=template/img/bg/blue.jpg&position=top&size=100% 20%
@title[Red]

@snap[north text-white span-100]
@size[2em](Redes)
@snapend

@snap[west split-screen-byline text-marino]
`$G = (V,A)$`
<br>
`$A =\{(i,j): i \in V, j \in V\}$`
@snapend

@snap[east split-screen-bigImg clip-me span-40]
![transcription](template/img/Figuras/networkModelsBarabasiNatureReviewGenetics2004.png)
@snapend

@snap[south-west template-note text-gray]
Redes
@snapend

+++?image=template/img/bg/blue.jpg&position=top&size=100% 20%
@title[Grafos]

@snap[north text-white span-100]
@size[2em](Direccionalidad)
@snapend

@snap[west split-screen-byline text-marino]
`$A =\{(i,j): i \in V, j \in V\}$`
<br><br>**Grafo no direccionado**<br>
`$(i,j) = (j,i)$`
<br>**Digrafo**<br>
`$(i,j) \not= (j,i)$`
@snapend

@snap[east split-screen-img]
![grafo](template/img/Figuras/Preparacion/redMinima.png)
@snapend

@snap[south-east split-screen-img]
![digrafo](template/img/Figuras/Preparacion/redMinimaDir.png)
@snapend

@snap[south-west template-note text-gray]
Redes
@snapend

+++?image=template/img/bg/blue.jpg&position=top&size=100% 20%
@title[Metricas]

@snap[north text-white span-100]
@size[1.5em](Métricas en redes)
@snapend

@snap[west split-screen-text text-marino span-50]
`$m$` Cantidad de aristas<br>
`$n$` Cantidad de vértices<br>
<br>
**Grafo no direccionado**
<br>
`$0\le m \le \frac{(n)(n-1)}{2}$`
<br>
**Digrafo**
<br>
`$0\le m \le n^{2}$`
@snapend

@snap[south-west template-note text-gray]
Redes
@snapend

+++?image=template/img/bg/blue.jpg&position=top&size=100% 20%
@title[Conectividad]

@snap[north text-white span-100]
@size[1.5em](Conectividad)
@snapend

@snap[west split-screen-text text-marino]
`$k_{i}^{in}$` Conectividad de entrada<br>
<br>
`$k_{i}^{out}$` Conectividad de salida<br>
<br>
`$p(k) \sim k^{-\gamma}$`
<br><br>
`$\log p(k_{i}=k) = -\gamma\log k + constante$`
@snapend
@snap[south-west template-note text-gray]
Redes
@snapend



